<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bills extends Model
{
    protected $table = 'bill_requests';

    use HasFactory;
    /**
     * Get the post that owns the comment.
     */
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }





}
