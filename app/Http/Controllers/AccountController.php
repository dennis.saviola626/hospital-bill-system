<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Models\User;
use App\Models\Bills;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Image;

class AccountController extends Controller
{
  public function account_home()
  {
    $data = Bills::where('approval', 3)->get();
    // dd($data);
    return view('account_dashboard')->with(['data' => $data]);
  }
  public function approves(request $request)
  {
    $id = $request -> id;
    $approve = Bills::where('id', $id)->first();
    $approve -> approval = 4;
    $approve ->save();

  }


  public function display_accountteam()
  {
    $status = $this -> approval;
    $data = Bills::where('approval', 3)->get();
          $response = [
            'status' => 1,
            'message' => 'Success',
          ];

          return view('account_table')->with(['data' => $data, 'approval'=>$status]);

  }

  public function rejects(request $request)
  {
    $id = $request -> id;
    $approve = Bills::where('id', $id)->first();
    $approve -> approval = 2;
    $approve ->save();

  }







}
