<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Models\User;
use App\Models\Bills;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Image;


class HRController extends Controller
{
    public function hr_home()
    {
      $data = Bills::where('approval', 1)->get();
      // dd($data);
      return view('hr_dashboard')->with(['data' => $data]);
    }


    public function approve_hr(request $request)
    {
      $id = $request -> id;
      $approve = Bills::where('id', $id)->first();
      $approve -> approval = 3;
      $approve ->save();
    }

    public function reject_hr(request $request)
    {
      $id = $request -> id;
      $approve = Bills::where('id', $id)->first();
      $approve -> approval = 2;
      $approve ->save();

    }

    public function display_hr_table()
    {
      $status = $this -> approval;
      $data = Bills::where('approval', 1)->get();
            $response = [
              'status' => 1,
              'message' => 'Success',
            ];

            return view('hr_table')->with(['data' => $data, 'approval'=>$status]);

    }
}
