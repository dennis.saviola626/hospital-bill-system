<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Models\User;
use App\Models\Bills;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Image;


class EmployeeController extends Controller
{
    public function employeehome()
    {
      $user = Auth::user()->id;
      $data = Bills::where('user_id', $user)->where('approval', 0)->count();
      return view('employee_dashboard')->with(['data' => $data]);
    }

    public function employeebill()
    {
      $user = Auth::user();
      return view('employeebill')->with(['user' => $user]);
    }

    public function create_bill(request $request)
    {
      $amount = $request -> amount;
      $reason = $request -> reason;
      $imagestring = $request -> imagefile->getClientOriginalName();

      $bills = new Bills;
      $bills->user_id=Auth::user()->id;
      $bills->amount = $amount;
      $bills->reason = $reason;



      if($request->hasFile('imagefile')){
          $image = $request ->file('imagefile');
          $location = public_path('images/' . $imagestring);
          Image::make($image)->resize(800,400)->save($location);


          $bills->image = $imagestring;
        }

      $bills->save();
      $user = Auth::user()->id;
      $data = Bills::where('user_id', $user)->count();
      return redirect('employeehome')->with(['data' => $data]);
    }

    public function pendingbill()
    {
      $user = Auth::user()->id;
      $data = Bills::where('user_id', $user)->where('approval', 0)->get();
      return view('pendingbill')->with(['data' => $data]);
    }

    public function approvedbill()
    {
      $user = Auth::user()->id;
      $data = Bills::where('user_id', $user)->whereIn('approval', [1,3,4])->get();

      $data_team = $this -> approval;
      return view('approvedbill')->with(['data' => $data, 'data_team' => $data_team]);
    }

    public function rejectedbills()
    {
      $user = Auth::user()->id;
      $data = Bills::where('user_id', $user)->where('approval', 2)->get();
      return view('rejectedbills')->with(['data' => $data]);
    }

    public function delete_bill(request $request)
    {
      $checked_id =   $request -> checked;
      $array = explode(',', $checked_id);

      $delete = Bills::whereIn('id', $array)->delete();

      return[
        'status' => 1,
        'message' => 'Successfully Deleted',
      ];
    }

    public function display_bills()
    {
      $user = Auth::user()->id;
      $data = Bills::where('user_id', $user)->where('approval', 2)->get();
            $response = [
              'status' => 1,
              'message' => 'Success',
            ];

            return view('bills_table')->with(['data' => $data]);

    }


}
