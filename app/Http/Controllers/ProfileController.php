<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Carbon\Carbon;
use Session;
use Auth;
use Illuminate\Support\Facades\Hash;



class ProfileController extends Controller
{
    public function editprofile(request $request)
    {
      $user = User::get();
      $user = auth()->user();
      $gendera = $this -> gender;
      return view('edit_profile')->with(['user'=>$user, 'gender'=>$gendera]);
    }

    public function updateprofile(request $request)
    {

          $user  = auth()->user();
          $username = $request->user_name;
          $email = $request->email;
          $mobile = $request->mobile;
          $address = $request->address;
          $gender = $request->gender;
          $date_of_birth = Carbon::parse($request->date_of_birth);

              if ($user) {
                User::where('user_name', $username)->update([
                  'email' => $email,
                  'mobile' =>$mobile,
                  'address' =>$address,
                  'gender' =>$gender,
                  'date_of_birth' =>$date_of_birth,
                ]);
              }
          return view('dashboard');
    }

    public function editpassword(request $request)
    {
      $data = $request->validate([
        'old_pass' => ['required'],
        'new_pass' => ['required'],
        're_enter' => ['required', 'same:new_pass']
      ]);
      $hashed = Auth::user()->password;
      if (Hash::check($request -> old_pass, $hashed)) {
        // code...
        $user = User::find(Auth::id());
        $user->password = Hash::make($request->new_pass);
        $user->save();
        Auth::logout();
        return view('welcome');
      }else{
        dd("Incorrect Password");
      }

    }









}
