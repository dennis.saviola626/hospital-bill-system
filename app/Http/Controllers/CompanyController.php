<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
class CompanyController extends Controller

{



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function post_login(request $request)
    {

      $user_name = $request->user_name;
      $request->validate([
          'user_name' => 'required',
          'password' => 'required',
      ]);
      $password_attempt = User::where('user_name', $user_name)->value('password_attempt');
      $status_value = User::where('user_name', $user_name)->value('status');
      $credentials = $request->only('user_name', 'password',);

      if (Auth::attempt($credentials) && $password_attempt < 5 && $status_value == 1) {
        $user = User::where('user_name', $user_name)->first();
        Auth::login($user);
        $this -> authenticated($user);
        $password_attempt = 0;
        $update_password_attempt = User::where('user_name', $user_name)->update(['password_attempt' => $password_attempt]);
        Session::flash('success', 'You have succesfully logged in');
        if (Auth::user()->type_id == '0')
          {
            return view('dashboard');  // admin dashboard path
          } elseif (Auth::user()->type_id == '4')  {
            return redirect()->route('employeehome');  // member dashboard path
          }  elseif (Auth::user()->type_id == '3'){
            return redirect()->route('tlhome'); // team leader dashboard path
          }  elseif (Auth::user()->type_id == '2'){
            return redirect()->route('hr_home');
          }  else{
            return redirect()->route('account_home');
          }
      }elseif ($status_value == 0 || $password_attempt >= 5) {
        $status_value = 0;
        $testing = User::where('user_name', $user_name)->update(['status' => $status_value]);
        $password_attempt = 0;
        $update_password_attempt_after_locked = User::where('user_name', $user_name)->update(['password_attempt' => $password_attempt]);
        Session::flash('locked', 'You are locked out from your account, please contact admin');
        return redirect('/');
      }else {
        $lock = User::where('user_name', $user_name)->value('password_attempt');
        $lock = $lock + 1;
        $test = User::where('user_name', $user_name)->update(['password_attempt' => $lock]);
        Session::flash('message','Wrong Credentials!');
        return redirect('/');
      }
    }

    public function dashboard(){
      $user = User::get();
      $usertype = $this -> typeid;
      $gender = $this -> gender;
      $status = $this -> status;
      return view('dashboard')->with(['user'=>$user, 'usertype'=>$usertype, 'gender'=>$gender, 'status'=>$status]);

    }

    public function authenticated($user) {
      $user->last_login_date = Carbon::now()->toDateTimeString();
      $user->save();
    }



}
