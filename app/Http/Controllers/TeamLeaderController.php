<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Carbon\Carbon;
use Session;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\Bills;

class TeamLeaderController extends Controller
{
    public function tlhome()
    {
      $user = User::all();
      $data = Bills::where('approval', 0)->get();
      $approval = $this -> approval;
      return view('tlhome')->with(['data' => $data, 'user'=>$user, 'approval' => $approval]);
    }

    public function approve(request $request)
    {
      $id = $request -> id;
      $approve = Bills::where('id', $id)->first();
      $approve -> approval = 1;
      $approve ->save();

    }


    public function display_teamlist()
    {
      $status = $this -> approval;
      $data = Bills::where('approval', 0)->get();
            $response = [
              'status' => 1,
              'message' => 'Success',
            ];

            return view('TeamLeaderTable')->with(['data' => $data, 'approval'=>$status]);

    }

    public function reject(request $request)
    {
      $id = $request -> id;
      $approve = Bills::where('id', $id)->first();
      $approve -> approval = 2;
      $approve ->save();

    }


















}
