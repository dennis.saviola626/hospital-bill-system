<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Session;
use DB;


class DashboardController extends Controller
{
  public function logout()
  {
    Auth::logout();
    return redirect('/');
  }

  public function display_list(){

          $usernamelist= User::get();
          foreach ($usernamelist as $key => $value) {
            $usernamelist[$key]['medical_ammount'] = DB::table("bill_requests")->where('user_id', $value->id )->sum('amount');
          }
          $usertype = $this -> typeid;
          $gender = $this -> gender;
          $status = $this -> status;
          $response = [
            'status' => 1,
            'message' => 'Success',
          ];
          return view('display_table')->with(['user' => $usernamelist, 'usertype'=>$usertype, 'gender'=>$gender, 'status'=>$status, 'response'=> $response ]);
  }

  public function update_list(request $request)
  {
    $id = $request->id;
    $user_type = $request->user_type;
    $email = $request->email;
    $mobile = $request->mobile;
    $address = $request->address;
    $gender = $request->gender;
    $date_of_birth = Carbon::parse($request->date_of_birth);
    $status = $request->status;

    $editvalidator = Validator::make($request->all(), [
    'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
    'mobile'=> ['required', 'string', 'min:8' , 'max:50'],
    'address'=>['required'],
    'date_of_birth' => ['required'],
    'status' => ['required'],
    ]);


        if ($editvalidator->fails()) {
          return [
              'status' => 0,
              'message' => $editvalidator->errors()->first(),
            ];
          $exist = User::where('id', $id)->first();
              if ($exist) {
                User::where('id', $id)->update([
                  'type_id' => $user_type,
                  'email' => $email,
                  'mobile' =>$mobile,
                  'address' =>$address,
                  'gender' =>$gender,
                  'date_of_birth' =>$date_of_birth,
                  'status' =>$status,
                ]);
              }
            }
    }

    public function create_user(request $request)
    {
      $user_name = $request->user_name;
      $type_id = $request->type_idx;
      $email = $request->email;
      $mobile = $request->mobile;
      $address = $request->address;
      $gender = $request ->gender;
      $date_of_birth = $request -> date_of_birth;
      $request->flashOnly(['username', 'email', 'mobile_number']);
      $username_old = $request->old('username');
      $password = $request->password;
      $exist = User::where('user_name',$user_name)->exists();
      $exist2 = User::where('email',$email)->exists();
      $exist3 = User::where('mobile',$mobile)->exists();
      if($exist == True || $exist2 == True || $exist3 == True){
        return [
            'status' => 0,
            'message' => 'Email / Username / Mobile Number already exist',
          ];
        }
        $validator = Validator::make($request->all(), [
        'user_name' => ['required', 'string', 'max:255','unique:users'],
        'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        'mobile'=> ['required', 'string', 'min:8' , 'max:50', 'unique:users'],
        'address'=>['required'],
        'date_of_birth' => ['required'],
        'password'=> ['required','min:8','regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)/'],
        'repassword' => ['required', 'same:password'],
        ]);
      if ($validator->fails()) {
        return [
            'status' => 0,
            'message' => $validator->errors()->first(),
          ];
        }
        User::create([
            'user_name' => $user_name,
            'type_id' => $type_id,
            'email' => $email,
            'mobile' => $mobile,
            'address' => $address,
            'gender' => $gender,
            'password_attempt' => 0,
            'status' => 1,
            'date_of_birth' => Carbon::parse($date_of_birth),
            'password' => Hash::make($password),
        ]);

        return [
                  'status' => 1,
                  'message' => 'Success',
                ];
      }


      public function delete_user($id)
      {
        $delete = User::where('id', $id)->delete();

        return[
          'status' => 1,
          'message' => 'Successfully Deleted',
        ];
      }


      public function search(request $request)
      {
        $usertype = $this -> typeid;
        $genderx = $this -> gender;
        $statusx = $this -> status;

        $type_id = $request-> type_id;
        $gender = $request-> gender;
        $status = $request-> status;
        $search = $request-> search;

        $filter = DB::table('users');

        if ($type_id != "") {
          // code...
          $filter = $filter->where('type_id', $type_id);
        }

        if ($gender != "") {
            $filter = $filter->Where('gender', $gender);
        }

        if ($status != "") {
          // code...
          $filter = $filter->Where('status' ,$status);
        }
        if ($search !="")
        {
          $filter = $filter->where('user_name','LIKE','%'.$search.'%');
        }
        $filter = $filter->get();

          return view('display_table')->with(['user'=>$filter, 'usertype'=>$usertype, 'gender'=>$genderx, 'status'=>$statusx])->withQuery($search);


      }













    }
