<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $typeid = [
      0 => 'Company',
      1 => 'Account Team',
      2 => 'HR',
      3 => 'Team Leader',
      4 => 'Employee',
    ];

    protected $gender = [
      0 => '-',
      1 => 'Male',
      2 => 'Female',
    ];

    protected $status = [
      0 => 'Locked',
      1 => 'Active',
    ];

    protected $approval = [
      0 => 'Pending',
      1 => 'Approved by TeamLeader',
      2 => 'Rejected',
      3 => 'Approved by HR',
      4 => 'Approved by Account Team',
    ];
}
