<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\TeamLeaderController;
use App\Http\Controllers\HRController;
use App\Http\Controllers\AccountController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::post('post_login', [CompanyController::class, 'post_login'])->name('post_login');
Route::get('dashboard', [CompanyController::class, 'dashboard'])->name('dashboard');

Route::get('editprofile', [ProfileController::class,'editprofile'])->name('editprofile');
Route::post('updateprofile', [ProfileController::class,'updateprofile'])->name('updateprofile');
Route::post('editpassword', [ProfileController::class,'editpassword'])->name('editpassword');




//admin
Route::middleware(['auth', 'isAdmin'])->group(function () {
  Route::get('userlist', [DashboardController::class, 'userlist'])->name('userlist');
  Route::post("logout",[DashboardController::class,"logout"])->name("logout");
  Route::get('display_list', [DashboardController::class, 'display_list'])->name('display_list');
  Route::post('update_list', [DashboardController::class, 'update_list'])->name('update_list');
  Route::post('create_user', [DashboardController::class, 'create_user'])->name('create_user');
  Route::delete('delete_user/{id}', [DashboardController::class, 'delete_user'])->name('delete_user');
  Route::post('search', [DashboardController::class,'search'])->name('search');
});

//employee
Route::middleware(['auth', 'isEmployee'])->group(function () {
  Route::get('employeehome', [EmployeeController::class,'employeehome'])->name('employeehome');
  Route::post('employeebill', [EmployeeController::class,'employeebill'])->name('employeebill');
  Route::post('create_bill', [EmployeeController::class,'create_bill'])->name('create_bill');
  Route::post('pendingbill', [EmployeeController::class,'pendingbill'])->name('pendingbill');
  Route::post('approvedbill', [EmployeeController::class,'approvedbill'])->name('approvedbill');
  Route::post('rejectedbills', [EmployeeController::class,'rejectedbills'])->name('rejectedbills');
  Route::post('delete_bill', [EmployeeController::class, 'delete_bill'])->name('delete_bill');
  Route::get('display_bills', [EmployeeController::class, 'display_bills'])->name('display_bills');
});


//team Leader
Route::middleware(['auth', 'isTeamLeader'])->group(function () {
  Route::get('tlhome', [TeamLeaderController::class,'tlhome'])->name('tlhome');
  Route::post('approve', [TeamLeaderController::class, 'approve'])->name('approve');
  Route::get('display_teamlist', [TeamLeaderController::class, 'display_teamlist'])->name('display_teamlist');
  Route::post('reject', [TeamLeaderController::class, 'reject'])->name('reject');
});

//HR
Route::middleware(['auth', 'isHR'])->group(function () {
  Route::get('hr_home', [HRController::class,'hr_home'])->name('hr_home');
  Route::post('approve_hr', [HRController::class, 'approve_hr'])->name('approve_hr');
  Route::post('reject_hr', [HRController::class, 'reject_hr'])->name('reject_hr');
  Route::get('display_hr_table', [HRController::class, 'display_hr_table'])->name('display_hr_table');
});

Route::middleware(['auth', 'isAccount'])->group(function () {
  Route::get('account_home', [AccountController::class,'account_home'])->name('account_home');
  Route::post('approves', [AccountController::class, 'approves'])->name('approves');
  Route::post('rejects', [AccountController::class, 'rejects'])->name('rejects');
  Route::get('display_accountteam', [AccountController::class, 'display_accountteam'])->name('display_accountteam');
});
