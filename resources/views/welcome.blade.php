<style media="screen">
form {
border: 3px solid #f1f1f1;
}

/* Full-width inputs */
input[type=text], input[type=password] {
width: 100%;
padding: 12px 20px;
margin: 8px 0;
display: inline-block;
border: 1px solid #ccc;
box-sizing: border-box;
}

/* Set a style for all buttons */
button {
background-color: #04AA6D;
color: white;
padding: 14px 20px;
margin: 8px 0;
border: none;
cursor: pointer;
width: 100%;
}

/* Add a hover effect for buttons */
button:hover {
opacity: 0.8;
}

/* Add padding to containers */
.container {
padding: 320px;
}

</style>

<form action="{{ route('post_login') }}" method="POST">
  @csrf
</div>
  <div class="container">
    <label for="uname"><b>Username</b></label>
    <input type="text" placeholder="Enter Username" name="user_name" required>

    <label for="psw"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="password" required>
    <button type="submit">Login</button>
    @if(session()->has('message'))
        <div class="alert alert-danger">
            {{ session()->get('message') }}
        </div>
    @endif
    @if(session()->has('locked'))
        <div class="alert alert-danger">
            {{ session()->get('locked') }}
        </div>
    @endif
  </div>
</form>
