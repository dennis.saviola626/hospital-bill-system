<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    @include('partials/_nav')
    <meta charset="utf-8">
    @yield('content')
  </head>
</html>
