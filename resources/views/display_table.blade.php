<!-- Table for user -->
<table class="table table-bordered">
  <thead>
        <tr>
          <th>#</th>
          <th>Username</th>
          <th>User Type</th>
          <th>Email</th>
          <th>Mobile</th>
          <th>Address</th>
          <th>Gender</th>
          <th>Created Date</th>
          <th>Last Login Date</th>
          <th>Date of Birth</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
  </thead>
  <tbody>
    @foreach($user as $key => $data)
      <tr>
          <td>{{ $key + 1 }}</td>
          <td>{{ $data->user_name }}</td>
          <td>
            @if(isset($usertype[$data->type_id]))
              {{ $usertype[$data->type_id] }}
            @endif
          </td>
          <td>{{ $data->email }}</td>
          <td>
            @if($data->mobile == null)
            <p class="text-danger fw-bold">No Data Found</p>
            @else
              {{ $data->mobile }}
            @endif
          </td>
          <td>
            @if($data->address == null)
              <p class="text-danger fw-bold">No Data Found</p>
            @else
              {{ $data->address }}
            @endif
          </td>
          <td>
            @if(isset($gender[$data->gender]))
              {{ $gender[$data->gender] }}
            @endif
          </td>
          <td>{{ $data->created_at }}</td>
          <td>{{ $data->last_login_date }}</td>
          <td>
            @if($data->date_of_birth == null)
            <p class="text-danger fw-bold">No Data Found</p>
            @else
            {{ $data->date_of_birth }}
            @endif
          </td>
          <td>@if(isset($status[$data->status]))
            {{ $status[$data->status] }}
          @endif</td>
          <td>
          <a <button type="button" class="btn btn-primary"  data-bs-toggle="modal" data-bs-target="#exampleEditModal{{ $data->id }}">
            Edit
          </button>
          </a>
            <div class="modal fade" id="exampleEditModal{{ $data->id }}" tabindex="-1" aria-labelledby="exampleModalLabel"
              aria-hidden="true">
              <div class="modal-dialog modal-xl">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit {{ $data->user_name }} Data</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>id</th>
                          <th>Username</th>
                          <th>User Type</th>
                          <th>Email</th>
                          <th>Mobile</th>
                          <th>Address</th>
                          <th>Gender</th>
                          <th>Created Date</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>{{ $data->id }}</td>
                          <td>{{ $data->user_name }}</td>
                          <td>
                          <select class="form-control user_type_{{ $data->id }}" id="select_user_type" class="" style="width: 200px">
                          <option value=0>Company</option>
                          <option value=1>Account Team</option>
                          <option value=2>HR Team</option>
                          <option value=3>Team Leader</option>
                          <option value=4>Employee</option>
                          </select>
                          </td>
                          <td><input class="email_{{ $data->id }}" type="text" value = "{{ $data->email }}"></td>
                          <td><input class="mobile_{{ $data->id }}" type="text" value = "{{ $data->mobile }}"></td>
                          <td><input class="address_{{ $data->id }}" type="textarea" value = "{{ $data->address }}"></td>
                          <td>
                            <select class="form-control gender{{ $data->id }}" id="select_gender_type" style="width: 200px">
                            <option value=0>-</option>
                            <option value=1>Male</option>
                            <option value=2>Female</option>
                            </select>
                          </td>
                          <td>{{ $data->created_at }}</td>
                        </tr>
                        <tr>
                          <th></th>
                          <th>Last Login Date</th>
                          <th>Date of Birth</th>
                          <th>Status</th>
                        </tr>
                        <tr>
                          <td></td>
                          <td>{{ $data->last_login_date }}</td>
                          <td><input class="date_of_birth_{{ $data->id }}" type="date" value = "{{ $data->date_of_birth }}"></td>
                          <td>
                            <select class="form-control status{{ $data->id }}" id="select_status_type" style="width: 200px">
                            <option>{{-- $data -> status --}}</option>
                            <option value=1>Active</option>
                            <option value=0>Locked</option>
                            </select>
                          </td>
                        </tr>

                      </tbody>
                    </table>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary edit_submit_button" modal-name=#exampleModal{{ $data->id }} hidden-id={{ $data->id }}>Save changes</button>
                  </div>
                </div>
              </div>
            </div>
            <a <button type="button" class="btn btn-danger"  data-bs-toggle="modal" data-bs-target="#exampleDeleteModal{{ $data->id }}">
              Delete
            </button>
          </a>
            <div class="modal fade" id="exampleDeleteModal{{ $data->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Warning</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                    Are you sure you want to delete this user?
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">NO</button>
                    <button type="button" class="btn btn-danger delete_button" modal-name=#exampleModal{{ $data->id }} hidden-id={{ $data->id }}>YES</button>
                  </div>
                </div>
              </div>
            </div>
          </td>

      </tr>
    @endforeach
  </tbody>
</table>

<script type="text/javascript">

  $(".edit_submit_button").click(function(){
    var id = $(this).attr('hidden-id')
    var user_type = $('.user_type_'+id).val()
    var email = $('.email_'+id).val()
    var mobile = $('.mobile_'+id).val()
    var address = $('.address_'+id).val()
    var gender = $('.gender'+id).val()
    var date_of_birth = $('.date_of_birth_'+id).val()
    var status = $('.status'+id).val()

    var fd=new FormData();
    fd.append("id", id);
    fd.append("user_type", user_type);
    fd.append("email", email);
    fd.append("mobile", mobile);
    fd.append("address", address);
    fd.append("gender", gender);
    fd.append("date_of_birth", date_of_birth);
    fd.append("status", status);

    $.ajax({
      url: '/update_list',
      type: "POST",
      data: fd,
      processData: false,
      contentType: false,
      success:function(data) {
// console.log(data)
// return;
        if (data['status'] == 0) {
          alert(data['message'])
          return;
        }
      $('body').removeClass('modal-open');
      $('.modal-backdrop').remove();
      display_list();
      }
    });
  })

  $(".delete_button").click(function(){
  var id = $(this).attr('hidden-id')

  var fd=new FormData();
  fd.append("id", id);

  $.ajax({
  url: '/delete_user/'+id,
  type: "DELETE",
  data: fd,
  processData: false,
  contentType: false,
  success:function(data) {
    console.log(data)
    return;
    if (data['status'] == 0) {
      alert(data['message'])
      return;
        }
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
        display_list();
      }
    })
  })


</script>
