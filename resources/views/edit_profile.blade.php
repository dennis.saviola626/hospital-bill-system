<html lang="en">
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
    </title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
</html>

<style media="screen">

body{margin-top:20px;}
.avatar{
width:200px;
height:200px;
}
</style>

<div class="container bootstrap snippets bootdey">
    <h2 class="text-primary">Edit Profile</h2>
      <hr>
	<div class="row">
      <!-- edit form column -->
      <form class="" action="{{ route('updateprofile') }}" method="POST">
        @csrf

        <div class="form-group">
          <label for="user_name">Username</label>
          <input type="text" class="form-control" name="user_name" id="user_name" value="{{ $user->user_name }}" readonly>
          <label for="email">Email</label>
          <input type="text" class="form-control" name="email" id="email" value="{{ $user->email }}">
          <label for="mobile">Mobile Number</label>
          <input type="text" class="form-control" name="mobile" id="mobile" value="{{ $user->mobile }}">
          <label for="address">Address</label>
          <input type="text" class="form-control" name="address" id="address" value="{{ $user->address }}">
          <label for="gender">Gender</label>
          @if(isset($gender[$user->gender]))
          @endif
          <select class="form-control gender" id="select_gender_type" style="width: 200px" >

          <option value=0>-</option>
          <option value=1 {{  $user->gender == 1 ? 'selected' : '' }} >Male</option>
          <option value=2 {{  $user->gender == 2 ? 'selected' : '' }}>Female</option>
          </select>
          <label for="date_of_birth">Date of Birth</label>
          <input type="date" class="form-control" name="date_of_birth" id="date_of_birth" value="{{ $user->date_of_birth }}" readonly>
          <br>
          <button type="submit" class="btn btn-success" name="button">Submit</button>
        </div>
      </form>
        <hr>
      <form class="" action="{{ route('editpassword') }}" method="POST">
          @csrf
        <h3 class="text-primary">Change Your Password</h3>
        <label for="user_name">Old Password</label>
        <input type="password" class="form-control old_pass" name="old_pass" id="old_pass" value="">
        <label for="user_name">New Password</label>
        <input type="password" class="form-control new_pass" name="new_pass" id="new_pass" value="">
        <label for="user_name">Re-enter New Password</label>
        <input type="password" class="form-control re_enter" name="re_enter" id="re_enter" value="">
        <br>
        <button type="submit" class="btn btn-success" name="button">Submit</button>
      </form>
      </div>
  </div>
</div>
