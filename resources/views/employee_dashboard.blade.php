@extends('main')
@section('content')

<style media="screen">
.button {
display: inline-block;
padding: 15px 25px;
font-size: 24px;
cursor: pointer;
text-align: center;
text-decoration: none;
outline: none;
color: #fff;
background-color: #4CAF50;
border: none;
border-radius: 15px;
box-shadow: 0 9px #999;
}

.button:hover {background-color: #3e8e41}

.button:active {
background-color: #3e8e41;
box-shadow: 0 5px #666;
transform: translateY(4px);
}
.container {
  height: 200px;

}

.vertical-center {
  margin-left: 550px;
  position: absolute;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
}
</style>

<body>
</body>
</html>
<div class="vertical-center">
  <div class="col-md-12 text-center">
    <div class="row">
      <form class="col" action="pendingbill" method="post">
        @csrf
        <button type="submit" class="button" name="pending">Pending Bill Request : {{ $data }}</button>

      </form>
      <form class="col" action="approvedbill" method="post">
        @csrf
        <button type="submit" class="button" name="approved">Approved Bill Request</button>
      </form>
      <form class="col" action="employeebill" method="post">
        @csrf
        <button type="submit" class="button" name="create">Create Bill Request</button>
      </form>
      <form class="col" action="rejectedbills" method="post">
        @csrf
        <button type="submit" class="button" name="create">Rejected Bill Request</button>
      </form>
    </div>
  </div>
</div>
@endsection
