@extends('main')
@section('content')

<!-- Table for user -->


<span class="TeamLeaderTable"></span>


@endsection

<script
  src="https://code.jquery.com/jquery-3.6.1.min.js"
  integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ="
  crossorigin="anonymous">
</script>


<script type="text/javascript">
display_teamlist();
function display_teamlist(){
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: '/display_teamlist',
      type: "GET",
      processData: false,
      contentType: false,
      success:function(data) {

        $('.TeamLeaderTable').html(data)

      },
      error: function(error) {
          console.log('eror',error.responseText)
      }
    });
  }
</script>
