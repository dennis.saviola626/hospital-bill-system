@extends('main')
@section('content')

<style media="screen">
  .modal-title{
    color:#000000;
  }
  .modal-body{
    color:#000000;
  }

</style>

<!DOCTYPE html>
<h1>Pending List ({{ Auth::user()->user_name }})</h1>
<table class="table table-dark table-striped">
  <tr>
  <th>#</th>
  <th>User ID</th>
  <th>Amount</th>
  <th>Reason</th>
  <th>Image</th>
  </tr>
  @foreach($data as $key => $datas)
  <tr>

  <th>{{ $key + 1 }}</th>
  <th>{{ $datas->user_id }}</th>
  <th>{{ $datas->amount }}</th>
  <th>{{ $datas->reason }}</th>

  <th>
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-link" data-bs-toggle="modal" data-bs-target="#exampleModal_{{$datas->id}}">
      {{ $datas->image }}
    </button>

    <!-- Modal -->
    <div class="modal fade modal-xl" id="exampleModal_{{$datas->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"  id="exampleModalLabel">Modal title</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <img src="{{asset("images/$datas->image")}}">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>
  </th>

  </tr>
  @endforeach
  <tr>

  </tr>
</table>

@endsection
