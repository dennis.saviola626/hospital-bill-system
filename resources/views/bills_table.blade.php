
<table class="table table-dark table-striped">
  <tr>
  <th>#</th>
  <th>User ID</th>
  <th>Amount</th>
  <th>Reason</th>
  <th>Image</th>
  <th>
    Action
    <div class="form-check">
      <input class="form-check-input selectAll" id="selectAll" type="checkbox" value="selectall_value" id="flexCheckDefault">
      <label class="form-check-label" for="flexCheckDefault">
        Select All
      </label>
    </div>
  </th>
  <th>
    <button class="check_values" type="button" name="button">check</button>
  </th>
  </tr>
  @foreach($data as $key => $datas)
  <tr>
  <th>{{ $key + 1 }}</th>
  <th>{{ $datas->user_id }}</th>
  <th>{{ $datas->amount }}</th>
  <th>{{ $datas->reason }}</th>
  <th>
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-link" data-bs-toggle="modal" data-bs-target="#exampleModal_{{$datas->id}}">
    {{ $datas->image }}
    </button>

    <!-- Modal -->
    <div class="modal fade modal-xl" id="exampleModal_{{$datas->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"  id="exampleModalLabel">Modal title</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <img src="{{asset("images/$datas->image")}}">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>

          </div>
        </div>
      </div>
    </div>
  </th>
  <th>
    <div class="form-check">
      <input class="form-check-input checkboxes" type="checkbox" value="{{$datas->id}}" name="check" id="flexCheckDefault">
      <label class="form-check-label" for="flexCheckDefault">
        Select to Delete
      </label>
    </div>
  </th>
  <th> </th>
  </tr>
  @endforeach
  <tr>

  </tr>
</table>

<script
  src="https://code.jquery.com/jquery-3.6.1.min.js"
  integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ="
  crossorigin="anonymous"></script>



<script type="text/javascript">

$(function() {
    $('#selectAll').click(function() {
        if ($(this).prop('checked')) {
            $('.checkboxes').prop('checked', true);
        } else {
            $('.checkboxes').prop('checked', false);
        }
    });

});
$(document).ready(function() {
        $(".check_values").click(function(){
            var checked = [];
            $.each($("input[name='check']:checked"), function(){
                checked.push($(this).val());

            var fd=new FormData();
            fd.append("checked", checked)
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
            url: '/delete_bill',
            type: "post",
            data: fd,
            processData: false,
            contentType: false,

            success:function(data) {
              if (data['status'] == 0) {
                alert(data['message'])
                return;
                  }
                  display_bills();

                }
              })
            });
        });

    });


</script>
