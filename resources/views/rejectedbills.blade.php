@extends('main')
@section('content')

<style media="screen">
  .modal-title{
    color:#000000;
  }
  .modal-body{
    color:#000000;
  }

</style>

<!DOCTYPE html>
<h1>Rejected List ({{ Auth::user()->user_name }})</h1>
<span class="bills_table"></span>


@endsection
<script
  src="https://code.jquery.com/jquery-3.6.1.min.js"
  integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ="
  crossorigin="anonymous">
</script>

<script type="text/javascript">
  display_bills();
  function display_bills(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
      url: '/display_bills',
      type: "GET",
      processData: false,
      contentType: false,
      success:function(data) {
        $('.bills_table').html(data)

      },
      error: function(error) {
          console.log('eror',error.responseText)
      }
    });
  }
</script>
