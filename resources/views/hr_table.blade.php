<table class="table table-dark table-striped">
  <tr>
  <th>#</th>
  <th>User ID</th>
  <th>Amount</th>
  <th>Reason</th>
  <th>Image</th>
  <th>Status</th>
  <th>Action</th>
  </tr>
  @foreach($data as $key => $datas)
  <tr>

  <th>{{ $key + 1 }}</th>
  <th>{{ $datas->user_id }}</th>
  <th>{{ $datas->amount }}</th>
  <th>{{ $datas->reason }}</th>

  <th>
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-link" data-bs-toggle="modal" data-bs-target="#exampleModal_{{$datas->id}}">
      {{ $datas->image }}
    </button>

    <!-- Modal -->
    <div class="modal fade modal-xl" id="exampleModal_{{$datas->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"  id="exampleModalLabel">Modal title</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            <img src="{{asset("images/$datas->image")}}">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>
  </th>
  <th>Approved By Team Leader</th>
  <th>
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#x{{$datas->id}}">
      Approve
    </button>

    <!-- Modal -->
    <div class="modal fade" id="x{{$datas->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Approve</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            Do you approve this? <br> <br>
            <img src="{{asset("images/$datas->image")}}">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">NO</button>
            <button type="button" class="btn btn-primary approve" data-bs-dismiss="modal" modal-name=#exampleModal{{ $datas->id }} hidden-id={{ $datas->id }}>YES</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#reject{{$datas->id}}">
      Reject
    </button>

    <!-- Modal -->
    <div class="modal fade" id="reject{{$datas->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Reject</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
            Do you want to reject this? <br> <br>
            <img src="{{asset("images/$datas->image")}}">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">NO</button>
            <button type="button" class="btn btn-primary reject_button" data-bs-dismiss="modal" modal-name=#exampleModal{{ $datas->id }} hidden-id={{ $datas->id }}>YES</button>
          </div>
        </div>
      </div>
    </div>
  </th>
  </tr>
  @endforeach
  <tr>

  </tr>
</table>


<script
  src="https://code.jquery.com/jquery-3.6.1.min.js"
  integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ="
  crossorigin="anonymous"></script>

<script type="text/javascript">
$(".approve").click(function(){
  var id = $(this).attr('hidden-id')

  var fd=new FormData();
  fd.append("id", id);
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  $.ajax({
    url: '/approve_hr',
    type: "POST",
    data: fd,
    processData: false,
    contentType: false,
    success:function(data) {

      if (data['status'] == 0) {
        alert(data['message'])
        return;
      }

    display_hr_table();
    }
  });
})

$(".reject_button").click(function(){
  var id = $(this).attr('hidden-id')

  var fd=new FormData();
  fd.append("id", id);
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  $.ajax({
    url: '/reject_hr',
    type: "POST",
    data: fd,
    processData: false,
    contentType: false,
    success:function(data) {

      if (data['status'] == 0) {
        alert(data['message'])
        return;
      }
    display_hr_table();
    }
  });
})
</script>
