<table class="table table-bordered">
  <thead>
        <tr>
          <th>#</th>
          <th>User ID</th>
          <th>Amount</th>
          <th>User Name</th>
          <th>Email</th>
          <th>Mobile</th>
          <th>Reason</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
  </thead>
  <tbody>
    @foreach($data as $key => $datas)
      <tr>
          <td>{{ $key + 1 }}</td>
          <td>{{ $datas->user_id }}</td>
          <td>{{ $datas->amount }}</td>
          <td>{{ $datas->users->user_name }}</td>
          <td>{{ $datas->users->email }}</td>
          <td>
            @if($datas->users->mobile == null)
            <p class="text-danger fw-bold">No Data Found</p>
            @else
              {{ $datas->users->mobile }}
            @endif
          </td>
          <td>{{ $datas -> reason }}</td>
          <td>
            @if(isset($approval[$datas->approval]))
            {{ $approval[$datas->approval] }}
            @endif
          </td>
          <td>
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#exampleModal_{{$datas->id}}">
              Approve
            </button>

            <!-- Modal -->
            <div class="modal fade" id="exampleModal_{{$datas->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Approve</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                    Are you sure?
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">NO</button>
                    <button type="button" class="btn btn-primary approve" data-bs-dismiss="modal" modal-name=#exampleModal{{ $datas->id }} hidden-id={{ $datas->id }}>YES</button>
                  </div>
                </div>
              </div>
            </div>

            <!-- Button trigger modal -->
            <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#reject">
              Reject
            </button>

            <!-- Modal -->
            <div class="modal fade" id="reject" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Reject</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <div class="modal-body">
                    Are you sure?
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">NO</button>
                    <button type="button" class="btn btn-primary reject_button" data-bs-dismiss="modal" modal-name=#exampleModal{{ $datas->id }} hidden-id={{ $datas->id }}>YES</button>
                  </div>
                </div>
              </div>
            </div>


          </td>
      </tr>
    @endforeach
  </tbody>
</table>

<script
  src="https://code.jquery.com/jquery-3.6.1.min.js"
  integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ="
  crossorigin="anonymous"></script>

<script type="text/javascript">
$(".approve").click(function(){
  var id = $(this).attr('hidden-id')

  var fd=new FormData();
  fd.append("id", id);
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  $.ajax({
    url: '/approve',
    type: "POST",
    data: fd,
    processData: false,
    contentType: false,
    success:function(data) {

      if (data['status'] == 0) {
        alert(data['message'])
        return;
      }

    display_teamlist();
    }
  });
})

$(".reject_button").click(function(){
  var id = $(this).attr('hidden-id')

  var fd=new FormData();
  fd.append("id", id);
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  $.ajax({
    url: '/reject',
    type: "POST",
    data: fd,
    processData: false,
    contentType: false,
    success:function(data) {

      if (data['status'] == 0) {
        alert(data['message'])
        return;
      }
    $("#reject").modal('hide');
    display_teamlist();
    }
  });
})
</script>
