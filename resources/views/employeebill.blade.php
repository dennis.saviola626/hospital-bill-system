@extends('main')
@section('content')

<head>


<div class="container">
  <h2>Employee Bill Form</h2>
  <p>Hi {{ $user->user_name }}, Please fill in the form below to create your bill</p>
  <table class="table table-hover">
    <thead>
      <tr>
        <th>Amount (MYR)</th>
        <th>Reason</th>
        <th>Image of Proof</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <form class="" action="create_bill" method="post" enctype="multipart/form-data">
          @csrf
          <td><input type="number" class="" name="amount" value=""></td>
          <td><textarea class="form-control" id="exampleFormControlTextarea1" name="reason" rows="3"></textarea></td>
          <td>
            <input class="form-control mt-3 imagefile" type="file" name="imagefile" id="formFile">
          </td>
          <td><button type="submit" class="btn btn-success mt-3" >Submit</button></td>

        </form>

      </tr>
    </tbody>
  </table>
</div>
</head>
@endsection
