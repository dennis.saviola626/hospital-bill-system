<html lang="en">
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
    </title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>
  </html>

  <head>
    <div class="float-end ms-auto p-2 bd-highlight">
      <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
          Hi, {{ Auth::user()->user_name }}
        </button>
        <ul class="dropdown-menu">
          <form action="{{ route('editprofile') }}" method="get">
                 @csrf
                 <button type="submit">Edit Profile</button>
          </form>
          <form action="{{ route('logout') }}" method="post">
                 @csrf
                 <button type="submit">Logout</button>
         </form>
       </ul>
     </div>
    </div>
  </head>
