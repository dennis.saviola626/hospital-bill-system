@extends('main')

@section('content')
<style media="screen">
.d-flex{
gap:5px;
}
div{
  margin-bottom: 10px;
}
label {
  display: inline-block;
  width: 150px;
}
select {
  display: inline-block;
}
.modal-xl {
    width: 100%;
   max-width:2600px;
}
.modal-content{
  width: auto;

}
textarea{
        vertical-align: text-top;
    }
}
</style>
<!-- nav bar -->
<nav>
  <div class="card">
      <div class="card-body">
          <div class="form-group d-flex bd-highlight mb-3">
              <strong>User Type :    </strong><br>
              <select id='status' class="form-control type_id" name="type_id" style="width: 200px">
                  <option value="">--Select User Type--</option>
                  <option value="0">Company</option>
                  <option value="1">Account Team</option>
                  <option value="2">HR Team</option>
                  <option value="3">Team Leader</option>
                  <option value="4">Employee</option>
              </select>

              <strong>Gender : </strong><br>
              <select id='status' class="form-control gender" name="gender" style="width: 200px">
                  <option value="">--Select Gender--</option>
                  <option value="1">Male</option>
                  <option value="2">Female</option>
              </select>

              <strong>Status : </strong><br>
              <select id='status' class="form-control status" name="status" style="width: 200px">
                  <option value="">--Select Status--</option>
                  <option value="1">Active</option>
                  <option value="0">Locked</option>
              </select>

              <input type="text" name="search" class="form-control search" name="search" style="width: 500px" placeholder="Search"><br>

              <button type="button" name="button" class="submits">Submit</button>

              </div>
          </div>
      </div>
  </div>
</div>
</nav>

<!-- create new user -->
<strong>User List</strong>
<button type="button" class="button" data-bs-toggle="modal" data-bs-target="#exampleModal">
Create New User
</button>
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg">
  <div class="modal-content ">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Create new user</h5>
      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <div class="container">
      <label> Username: </label>
      <input type="text" name="user_name" class="user_name"/> <br> <br>
      <div class="form-group ">
        <label for="exampleFormControlSelect1">Select User Type: </label>
        <select class="form-control type_idx" id="select_user_type" style="width: 200px">
        <option value = 0>Company</option>
        <option value = 1>Account Team</option>
        <option value = 2>HR Team</option>
        <option value = 3>Team Leader</option>
        <option value = 4>Employee</option>
        </select>
      <label>
      Email :
      </label>
      <input type="text" name="email" class="email"/> <br> <br>
      <label>
      Mobile Number :
      </label>
      <input type="text" name="mobile" class="mobile"/> <br> <br>
      <label>
      Address:
      </label>
      <textarea id="address" name="address" class="address" rows="4" cols="50"></textarea> <br><br>
      <label for="exampleFormControlSelect1">Select Gender: </label>
      <select class="form-control gender" id="select_user_type" style="width: 200px">
      <option value = 1>Male</option>
      <option value = 2>Female</option>
      </select>

      <label>
      Date of Birth:
      </label>
      <input type="date" id="date_of_birth" class="date_of_birth" name="date_of_birth"> <br> <br>
      <label>
      Password :
      </label>
      <input type="password" id="password" class="password" name="password"> <br> <br>
      <label>
      Re-type Password :
      </label>
      <input type="password" id="repassword" class="repassword" name="repassword"> <br> <br>
      <div class="pt-2">
      <button class="w-40 btn btn-sm btn-success submit_new_user" type="button">submit</button>
      </div>
    </div>
  </div>
</div>
</div>
</div>

<!-- display table -->

<span class="display_table"></span>


<!-- jquery script -->
<script
  src="https://code.jquery.com/jquery-3.6.1.min.js"
  integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ="
  crossorigin="anonymous"></script>


<!-- ajax script -->
<script type="text/javascript">
display_list();
function display_list(){
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  $.ajax({
    url: '/display_list',
    type: "GET",
    processData: false,
    contentType: false,
    success:function(data) {
      $('.display_table').html(data)

    },
    error: function(error) {
        console.log('eror',error.responseText)
    }
  });
}

//filter Search
$(".submits").click(function(){
  var type_id = $('.type_id').val()
  var gender = $('.gender').val()
  var status = $('.status').val()
  var search = $('.search').val()

  var fd=new FormData();
  fd.append("type_id", type_id);
  fd.append("gender", gender);
  fd.append("status", status);
  fd.append("search", search);

  $.ajax({
  url: '/search',
  type: "POST",
  data: fd,
  processData: false,
  contentType: false,
  success:function(data) {
  // console.log(data)
  // return;
    if (data['status'] == 0) {
        alert(data['message'])
        return;
      }else{
        $('.display_table').html(data)
      }
    }
  });
});

// create new Account
$(".submit_new_user").click(function(){

  // console.log('ads')
  // return;
  var user_name = $('.user_name').val()
  var type_id = $('.type_idx').val()
  // console.log(type_id)
  // return;
  var email = $('.email').val()
  var mobile = $('.mobile').val()
  var address = $('.address').val()
  var gender = $('.gender').val()
  var date_of_birth = $('.date_of_birth').val()
  var password = $('.password').val()
  var repassword = $('.repassword').val()

  var fd=new FormData();
  fd.append("user_name", user_name);
  fd.append("type_idx", type_id);
  fd.append("email", email);
  fd.append("mobile", mobile);
  fd.append("address", address);
  fd.append("gender", gender);
  fd.append("date_of_birth", date_of_birth);
  fd.append("password", password);
  fd.append("repassword", repassword);

  $.ajax({
  url: '/create_user',
  type: "POST",
  data: fd,
  processData: false,
  contentType: false,
  success:function(data) {
    console.log(data)
    return;

    if (data['status'] == 0) {
        alert(data['message'])
        return;
      }else{
        $('.user_name').val("")
        $('.type_id').val("")
        $('.email').val("")
        $('.mobile').val("")
        $('.address').val("")
        $('.gender').val("")
        $('.date_of_birth').val("")
        $('.password').val("")
        $('.repassword').val("")
        $('#exampleModal').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
        display_list();
      }
    }
  });
});
</script>
@endsection
